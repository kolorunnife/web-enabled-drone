from flask import Flask, render_template, url_for, Response, request
from forms import ControlForm
import ardrone_api as drone
import cv2
import sys

app = Flask(__name__)

app.config['SECRET_KEY'] = 'c2617f5d8242e291fa1845b976d59a27'

drone.setup_drone()

@app.route("/")
def main():
    form = ControlForm()
    return render_template('index.html', form=form)

#Handles the video livestreaming to html page
def gen():
    haar_cascade_face = cv2.CascadeClassifier(cv2.data.haarcascades + 'frontalface.xml')
    cam = cv2.VideoCapture(0)

    running = True

    while running:
        _, frame = cam.read()

        img = None

        if (drone.FACE_DETECTION_MODULE):
            img = drone.face_detection_module(haar_cascade_face, frame)
        else:
            img = drone.video_feed_module(frame)

        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + img + b'\r\n\r\n')

    cam.release()

@app.route('/video_feed')
def video_feed():
    return Response(gen(),
                    mimetype='multipart/x-mixed-replace; boundary=frame')


#Handle POST Request for controlling drone
@app.route("/control/data")
def control_data():
    data = request.args.get('inputData')

    if (data == "reset"):
        drone.drone.reset()
        return data

    drone.set_dir(data)

    print('Setting Direction To: ' + drone.DIRECTION, file=sys.stderr)

    drone.start_movement()

    return drone.DIRECTION


@app.route('/control/switch-cam', methods=['POST'])
def control_switch_cam():
    form = ControlForm()

    if (form.inputData.data == "0"):
        drone.drone.set_cam(1)
        return "1"
    else:
        drone.drone.set_cam(0)
        return "0"

@app.route('/control/face-detection', methods=['POST'])
def control_face_detection():
    form = ControlForm()

    if (form.inputData.data == "off"):
        drone.FACE_DETECTION_MODULE = True
        return "on"
    else:
        drone.FACE_DETECTION_MODULE = False
        return "off"

@app.route('/control/status', methods=['POST'])
def control_status():
    form = ControlForm()

    if (form.inputData.data == "land"):
        drone.land()
        return "success"

    elif (form.inputData.data == "takeoff"):
        drone.takeoff()
        return "success"