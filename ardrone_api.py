##################################################################
# LIBARAY                                                        #
#                                                                #
# Credits to: http://www.playsheep.de/drone/downloads.html       #
#                                                                #
##################################################################
import sys
import cv2
import ardrone
import _thread

drone = ardrone.ARDrone()
global FACE_TRACKER_MODULE, MANUAL_CONTORL_MODULE, LAST_DIRECTION, DIRECTION, MOVING, MOVING_THREAD

FACE_TRACKER_MODULE = False,
MANUAL_CONTORL_MODULE = False
FACE_DETECTION_MODULE = False
DIRECTION = None
LAST_DIRECTION = None
MOVING = False
MOVING_THREAD = None


def setup_drone():
    drone.reset()
    drone.set_cam(0)


def video_feed_module(frame):
    ret, jpeg = cv2.imencode('.jpg', frame)

    return jpeg.tobytes()


def face_detection_module(haar_cascade_face, frame):
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces_rects = haar_cascade_face.detectMultiScale(gray, 1.2, 5)

    for (x, y, w, h) in faces_rects:
        frame = cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

    ret, jpeg = cv2.imencode('.jpg', frame)

    return jpeg.tobytes()


def takeoff():
    drone.set_speed(1)
    drone.takeoff()

def forward(speed):
    drone.set_speed(speed)
    drone.move_forward()


def backward(speed):
    drone.set_speed(speed)
    drone.move_backward()


def turn_right(speed):
    drone.set_speed(speed)
    drone.turn_right()


def turn_left(speed):
    drone.set_speed(speed)
    drone.turn_left()

def up(speed):
    drone.move_up()

def down(speed):
    drone.move_down()

def hover():
    drone.hover()

def land():
    drone.land()

def set_dir(dir):
    global DIRECTION
    DIRECTION = dir

def start_movement():
    global MOVING, MOVING_THREAD
    if (MOVING == False):
        MOVING = True
        MOVING_THREAD = _thread.start_new_thread(_move, ())

def _move():
    spd = 1
    global MOVING, DIRECTION, LAST_DIRECTION
    while MOVING:
        if DIRECTION == LAST_DIRECTION:
            continue

        if DIRECTION == 'w':
            forward(spd)
        if DIRECTION == 'a':
            turn_left(spd)
        if DIRECTION == 's':
            backward(spd)
        if DIRECTION == 'd':
            turn_right(spd) 
        if DIRECTION == 'up':
            up(spd)
        if DIRECTION == 'down':
            down(spd)                      
        else:
            hover()

        LAST_DIRECTION = DIRECTION
        print('>>>>>>>>>>>> Direction: ' + DIRECTION + ' <<<<<<<<<<<<<<<', file=sys.stderr)

def get_battery_percent():
    return str(drone.navdata)
